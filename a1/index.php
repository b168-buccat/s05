<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S05: Client-Server Communication (Basic To-Do)</title>
</head>
<body>
	<?php session_start(); ?>

	<h3>Login/Logout</h3>

	<?php if (isset($_SESSION['isLoggedIn'])): ?>
		<?php if($_SESSION['isLoggedIn'] === true): ?>

			<?php echo "Hello, johnsmith@gmail.com"?>
			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="logout"/>
				<button type="submit">Logout</button>
			</form>
		<?php endif; ?>
		
		<?php if($_SESSION['isLoggedIn'] === false): ?>
			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="login"/>
				username: <input type="email" name="username" required/>
				password: <input type="password" name="password" required/>
				<button type="submit">Login</button>
			</form>
		<?php endif; ?>
	<?php endif; ?>
</body>
</html>