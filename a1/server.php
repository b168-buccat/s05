<?php

session_start();

class LoginUser {
	public function login($usernameInput, $passwordInput) {
		if ($usernameInput === 'johnsmith@gmail.com' && $passwordInput === '1234') {
			$_SESSION['isLoggedIn'] = true;
		}
	}

	public function logout() {
		$_SESSION['isLoggedIn'] = false;
	}
}

$loginUser = new LoginUser();

if($_POST['action'] === 'login') {
	$loginUser->login($_POST['username'], $_POST['password']);
}
else if ($_POST['action'] === 'logout') {
	$loginUser->logout();
}

header('Location: ./index.php');